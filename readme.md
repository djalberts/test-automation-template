# Test Automation Template Project
## Project Overview

Welcome to the Test Automation Template Project! This project provides a structured foundation for creating and running automated tests using Robot Framework. The framework is designed for web testing, API testing, and mobile testing, and supports parallel test execution.

## Index

1. [Initiate Virtual Environment](#initiate-virtual-environment)
2. [Framework to Install](#framework-to-install)
3. [Project Structure](#project-structure)
4. [Python Packaging](/doc/packaging/python_packing.md)
5. [Start Tests](#start-tests)

## Initiate Virtual Environment

To maintain project dependencies and isolate them from other projects, a virtual environment is recommended.

Install Virtual Environment for Python:

`pip install virtualenv`

Setup Virtual Environment for the Project:

`virtualenv <name-venv>`

Activate the Virtual Environment

`source ./venv/bin/activate`

## Framework to Install

The following Robot Framework and related libraries are the core components of this test automation template and are installed with pip in the virtual environment:

- robotframework: Core framework for test automation.
- robotframework-seleniumlibrary: Web testing library.
- robotframework-requests: Library for API testing.
- robotframework-appiumlibrary: Library for mobile testing.
- robotframework-pabot: Enables running tests in parallel.
- robotframework-databaselibrary: Provides keywords for interacting with databases.

## Project Structure

- `tests`: Test suites are organized in the tests/ folder.
- `resources`: Reusable keywords, along with Python keyword libraries (.py files), are stored in the resources/ folder.
- `libraries`: Custom Python keyword libraries can be stored in a separate libraries/ folder if needed.
- `data`: Test data files, including Python or YAML variable files, are organized in the data/ folder.

An example of a project structure can be found [here](doc/project_structure.md).

## Start Tests

Execute the following command to run tests when the virtualenv is activated:

`robot --pythonpath . -d ./results tests/`

This command initiates test execution with the specified project structure, and the results will be stored in the results/ folder.

## Getting Started

1. Clone this repository.
2. Create and activate a virtual environment.
3. Install required libraries using pip install -r requirements.txt.
4. Customize the project structure, tests, and resources based on your requirements.
5. Run tests using the provided command.

Feel free to explore and enhance this template to suit your specific testing needs!

Happy testing!