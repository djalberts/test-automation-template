*** Settings ***
Library    ./libraries/TestLibrary/TestLibrary.py    Flash37    
Library    ./libraries/MyLibrary/MyLibrary.py        DJ    

*** Test Cases ***

My First Robot Test
    Log To Console            Hello Robot World!
    Print The Test Name
    Print The Actual Name

*** Keywords ***
Print The Test Name
    [Documentation]    This prints the test name
    Print Test Name

Print The Actual Name
    [Documentation]    This print the actual name
    Print Name