import os
import subprocess
from grpc_tools import protoc
from fnmatch import fnmatch
from pathlib import Path
import sys

if sys.version_info >= (3, 9, 0):
    from importlib import resources
else:
    import pkg_resources


"""
This file contains functions that can be used for the generation of python classes from protobuf files.
By default it will dynamically go through the proto directory and will generated python code in the generated directory.
"""


PROTO_DIR_NAME = "proto"
PROTO_EXT = ".proto"
GENERATED_DIR_NAME = "generated"


def get_proto_files(path):
    """
    Get all the protobuf files from the specified directory and its subdirectories.

    Parameters:
        path (Path): The path where the protobuf files reside.

    Returns:
        list[Path]: List of absolute paths to different protobuf files.
    """
    return [Path(root) / name for root, _, files in os.walk(path) for name in files if name.endswith(PROTO_EXT)]


def get_proto_directories(path):
    """
    Get all the directories and subdirectories where protobuf files reside.

    Parameters:
        path (Path): The path where the protobuf files reside.

    Returns:
        list[Path]: List of absolute paths to different directories where protobuf files reside.
    """
    return [Path(root) for root, _, files in os.walk(path) if any(fnmatch(file, f'*{PROTO_EXT}') for file in files)]


def filter_parent_directories(root_directory, directories):
    """
    Filter parent directories from a list of directories and return them sorted.

    Args:
        root_directory (Path): Path of the root proto directory.
        directories (list): A list of pathlib.Path objects representing directory paths.

    Returns:
        list: A list of pathlib.Path objects representing the filtered parent directories.
    """
    root_directory = os.path.normpath(root_directory)
    parent_directories = set()

    for directory in directories:
        highest_parent = os.path.normpath(directory)
        for path in directories:
            if directory != path:
                common_path = os.path.normpath(os.path.commonpath([directory, path]))
                if common_path != highest_parent and common_path != root_directory:
                    highest_parent = common_path
        parent_directories.add(Path(highest_parent))

    return sorted(parent_directories)


def create_init_files(start_path, end_path):
    """
    Go from start_path to end_path and create __init__.py files using touch when none exist.

    Args:
    start_path (Path): The starting directory path.
    end_path (Path): The ending directory path.
    """
    init_files = ["__init__.py", "__init__.pyi"]

    end_path.mkdir(parents=True, exist_ok=True)

    for directory in list(end_path.parents):
        if directory == start_path:
            return

        # Create __init__.py files if they don't exist
        for init_file in init_files:
            init_path = directory / init_file
            if not init_path.exists():
                init_path.touch()


def get_resource_file_name(package_or_requirement: str, resource_name: str) -> str:
    """Obtain the filename for a resource on the file system."""
    file_name = None
    if sys.version_info >= (3, 9, 0):
        file_name = (
            resources.files(package_or_requirement) / resource_name
        ).resolve()
    else:
        file_name = pkg_resources.resource_filename(
            package_or_requirement, resource_name
        )
    return str(file_name)


def run_protoc(src, dst):
    """
    Run protoc on the specified directory and generate protobuf classes.

    Parameters:
        src (Path): The path where the protobuf files reside.
        dst (Path): The path where the generated protobuf classes should be generated.
    """
    print(f"Running protoc on: {src}")

    files = get_proto_files(src)
    proto_include = get_resource_file_name("grpc_tools", "_proto")

    for file in files:
        cmd = [
            'grpc_tools.protoc',
            f'-I{proto_include}',
            f'-I{src}',
            f'--python_out={dst}',
            f'--grpc_python_out={dst}',
            str(file)
        ]

        protoc.main(cmd)


def run_protol(src, dst):
    """
    Run the protol tool and fix broken imports for the generated protoc python code.

    Parameters:
        src (Path): The path where the protobuf files reside.
        dst (Path): The path where the generated protobuf classes should reside.
    """
    print(f"Running protol on {dst}")

    directories = get_proto_directories(src)

    proto_include = get_resource_file_name("grpc_tools", "_proto")

    for directory in directories:
        subprocess.run(f'protol --create-package --in-place --python-out {dst} ' + 
                       f'protoc --proto-path={src} --proto-path={proto_include} {directory}/*{PROTO_EXT}', shell=True)


def main():
    root_directory = Path(os.path.dirname(__file__))
    source_directories = filter_parent_directories(root_directory / PROTO_DIR_NAME, get_proto_directories(root_directory / PROTO_DIR_NAME))
    generated_directories = [Path(str(directory).replace(PROTO_DIR_NAME, GENERATED_DIR_NAME)) for directory in source_directories]

    # Create init files in generated directories
    for generated_dir in generated_directories:
        print("Will be generated in: " + str(generated_dir))
        create_init_files(root_directory, generated_dir)

    # Run protoc and protol on all source and generated directories.
    for source_dir, generated_dir in zip(source_directories, generated_directories):
        run_protoc(source_dir, generated_dir)
        run_protol(source_dir, generated_dir)


if __name__ == '__main__':
    main()
