from robot.api.deco import keyword, library

class TestLibrary:
  ROBOT_LIBRARY_SCOPE='TEST CASE'
  ROBOT_LIBRARY_VERSION = 0.1
  
  def __init__(self, name):
    self.name = name

  @keyword('Print Test Name')
  def print_test_name(self):
    print("Hello my test name is {}".format(self.name))