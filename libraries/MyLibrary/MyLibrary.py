from robot.api.deco import keyword

class MyLibrary:
  ROBOT_LIBRARY_SCOPE='TEST CASE'
  ROBOT_LIBRARY_VERSION = 0.1
  
  def __init__(self, name):
    self.name = name

  @keyword('Print Name')
  def print_name(self):
    print("Hello my name is {}".format(self.name))