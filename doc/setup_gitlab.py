from setuptools import setup, find_packages
import os

version = os.getenv("CI_COMMIT_TAG")
author = os.getenv("GITLAB_USER_NAME")
author_email = os.getenv("GITLAB_USER_EMAIL")

setup(
  name='proto_compiled_lib',
  version=version,
  author=author,
  author_email=author_email,
  description='A simple hello world protobuf project example package.',
  packages=find_packages(),
  classifiers=[
    'Programming Language :: Python :: 3',
    'License :: OSI Approved :: MIT License',
    'Operating System :: OS Independent',
  ],
  python_requires='>=3.11',
)