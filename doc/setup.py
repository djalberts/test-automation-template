from setuptools import setup, find_packages

setup(
  name='proto_compiled_lib',
  version='0.1.0',
  author='Dennis J. Alberts',
  author_email='dennis.alberts@hotmail.com',
  description='A simple hello world protobuf project example package.',
  packages=find_packages(),
  classifiers=[
    'Programming Language :: Python :: 3',
    'License :: OSI Approved :: MIT License',
    'Operating System :: OS Independent',
  ],
  python_requires='>=3.11',
)