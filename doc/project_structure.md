```log
├── bin
│   └── # Place your external binaries & executable here
├── config
│   └── conda.yaml
├── devdata
│   ├── env.json
│   └── # A place for your development phase test data
├── .gitignore
├── libraries
│   └── # Collect your library files here
├── LICENSE
├── output
│   └── # Having a standard place for outputs is always good
├── README.md # Mark-down readme in the root is a good place to describe what the thing does.
├── resources
│   └── #A common style in Robot Framework is to place your keyword implementations in one location
├── robot.yaml # The must-have configuration file in the root
├── tasks
│   └── # Another common style in Robot Framework is to place your robot task implementations in one location
├── temp
│   └── # Always good to have one standard place for temp files
└── variables
    └── # A place for your variable definitions makes these easy to find and manage
```