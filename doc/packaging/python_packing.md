## Python packing for generated protobuf classes

Create a file in the project directory called "setup.py" to define the package metadata:

```
from setuptools import setup, find_packages

setup(
  name="<package_name>",
  version="0.1.0",
  description="A simple Python package for math operations and geometry calculations",
  packages=find_packages(),
  classifiers=[
    "Programming Language :: Python :: 3",
    "License :: OSI Approved :: MIT License",
    "Operating System :: OS Independent",
  ],
  python_requires=">=3.6",
)
```

The setup.py can also be copied from `doc/setup.py.` to the parent directory of the to be created package. `<package_name>` should only underscores ( _ ) and not dashes( - ).

Build the package using the following command in the terminal:

```
python -m build
```

Twine can be used to upload the package to a repository:

```
twine upload --repository <repo_name> <project_dir>/dist/*
```
